"""
Machine Learning 2 HomeWork 1 ->

[Marks 50] #1. Write a Hill-Climbing algorithm to find the maximum
value of a function f, where

f = |13 * one(v) - 170|.

Here, v is the input binary variable of 40 bits and the one counts
the number of ‘1’s in v. Set MAX =100, and thus reset algorithm 100
times for the global maximum and print the found maximum-value for
each reset separated by a comma in the Output.txt file.

[Marks 50] #2. Write a Simulated-Annealing algorithm to find the
maximum value of a function f, where

f = |14 * one (v) -190|.

Here, v is the input binary variable of 50 bits and the one counts the
number of ‘1’s in v. Set MAX =200, and thus reset algorithm 200 times
for the global maximum and print the found maximum-value for each
reset separated by a comma in the Output.txt file.


Bonus ->

"""

# Generate -> Test -> Mutate|
# ^-------<-------<---------v
""" IMPORTS """
import secrets
import random
from math import exp
import matplotlib.pyplot as plt   # For data visualization
import sys

# Printing the data to the 'output.txt' file
f = open('output.txt', 'w')
sys.stdout = f


""" METHODS """
def one(v):
    """ Returns the number of 'set bits' in each string """
    c = 0
    for bit in v:
        if bit == '1': c += 1
    return c


def funcA(x):
    """ Hill-Climbing function """
    return abs(13 * one(x) - 170)


def funcB(x):
    """ Simulated-Annealing function """
    return abs(14 * one(x) - 190)


def mutation(v):
    """ Returns a set of mutated strings each having 1-Bit different from the original string """
    neighbors = set([])
    while len(neighbors) < len(v):
        for x in range(len(v)):
            if v[x] == '1': new = v[:x] + '0' + v[x+1:]
            else: new = v[:x] + '1' + v[x+1:]
            neighbors.add(new)
    return neighbors


def hillclimber():
    """ Hill-Climber that repeats 100 times using funcA from above as the eval function """
    output = []   # solutions
    iterations = 0 # t <- 0
    while iterations != 100: # repeat1
        local = 0 # local <- false
        init = ''.join(secrets.choice('01') for _ in range(40)) # TRNG
        currentvalue = funcA(init)
        while not local:
            # Create 40 neighbors of the string
            neighs = mutation(init)

            # Select the best neighbor
            bestneigh = bestvalue = 0
            for i in neighs:
                value = funcA(i)
                if value > bestvalue: bestvalue, bestneigh = value, i

            # Compare to original string
            if currentvalue < bestvalue: currentvalue, init = bestvalue, bestneigh
            else: local = 1

        # Save the best solution and start the algorithm with a new random string
        output.append(currentvalue)
        iterations += 1
    return output


def simulatedannealing():
    """ Simulated-Annealing function that repeats 200 times using funcB from above as the eval function """
    output = []
    iterations = 0
    while iterations != 200:
        temp = 200                                               # Initial temperature
        init = ''.join(secrets.choice('01') for _ in range(50))  # TRNG to create a random string of {0,1} with len(50)
        currentvalue = funcB(init)                               # Value of the evaluated string
        while temp > 5:                                          # Repeat with the current variables until temp > 20
            # for _ in range(5): Reruns each temp and string 35 times(useful in advanced algos, but not in this one)
            neighs = mutation(init)                              # Create 50 neighbors of the string, each 1 bit diff

            # Select best neighbor from neighs
            bestneigh = bestvalue = 0
            for i in neighs:
                value = funcB(i)
                if value > bestvalue: bestvalue, bestneigh = value, i

            # Compare to the original string and swap if the neighbor is equal or better
            if currentvalue <= bestvalue: currentvalue, init = bestvalue, bestneigh
            # A chance of selecting a worse solution
            elif currentvalue != 510 and random.random() < exp((bestvalue - currentvalue)/temp): currentvalue = bestvalue

            temp *= 0.9  # Shrink the temp and go again on the same random string
        # Save the best solution and start the algorithm with a new random string
        output.append(currentvalue)
        iterations += 1
    return output


""" DATA REPRESENTATION AND OUTPUT """
hilldata = hillclimber()
simdata = simulatedannealing()

# Plots the hill climbing data along with printing out the data(the data is redirected to 'output.txt')
for x in range(100):
    plt.plot(x, hilldata[x], '.b')
    plt.title('Hill Climber Data')
    plt.xlabel('Iteration'); plt.ylabel('Value')

print(); print('Hill Climber results'); print(hilldata); plt.show()

# Plots the simulated annealing data along with printing out the data(the data is redirected to 'output.txt')
for x in range(200):
    plt.plot(x, simdata[x], '.b')
    plt.title('Simulated Annealing Data')
    plt.xlabel('Iteration'); plt.ylabel('Value')

print(); print('Simulated Annealing results'); print(simdata); plt.show()
f.close()